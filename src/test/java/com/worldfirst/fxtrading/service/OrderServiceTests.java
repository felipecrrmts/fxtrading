package com.worldfirst.fxtrading.service;

import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderSummary;
import com.worldfirst.fxtrading.model.OrderType;
import com.worldfirst.fxtrading.model.builder.OrderBuilder;
import com.worldfirst.fxtrading.repository.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTests {

    private final static Long ID = 123L;
    @Value("${order.currency}")
    private String GBP_USD;
    @Value("${order.current.price.gbp.usd.value}")
    private String PRICE;
    private Order orderBid5000, otherAsk6050, otherAsk6040;
    @Mock
    private OrderRepository orderRepository;
    @InjectMocks
    OrderService orderService;

    @Before
    public void setup(){
        ReflectionTestUtils.setField(orderService, "defaultPrice", PRICE);
        setupOrder();
    }

    private void setupOrder(){
        orderBid5000 = new OrderBuilder()
                .withUserId(UUID.randomUUID().toString())
                .withOrderType(OrderType.BID)
                .withCurrency(GBP_USD)
                .withPrice(new BigDecimal(PRICE).setScale(4, BigDecimal.ROUND_HALF_UP))
                .withAmount(new BigDecimal("5000").setScale(4, BigDecimal.ROUND_HALF_UP))
                .build();
        otherAsk6050 = new OrderBuilder()
                .withUserId(UUID.randomUUID().toString())
                .withOrderType(OrderType.ASK)
                .withCurrency(GBP_USD)
                .withPrice(new BigDecimal(PRICE).setScale(4, BigDecimal.ROUND_HALF_UP))
                .withAmount(new BigDecimal("6050").setScale(4, BigDecimal.ROUND_HALF_UP))
                .build();
        otherAsk6040 = new OrderBuilder()
                .withUserId(UUID.randomUUID().toString())
                .withOrderType(OrderType.ASK)
                .withCurrency(GBP_USD)
                .withPrice(new BigDecimal(PRICE).setScale(4, BigDecimal.ROUND_HALF_UP))
                .withAmount(new BigDecimal("6040").setScale(4, BigDecimal.ROUND_HALF_UP))
                .build();
    }

    @Test
    public void shouldSaveOrder(){
        given(orderRepository.save(any(Order.class))).willReturn(orderBid5000);
        Order savedOrder = orderService.save(orderBid5000);
        assertEquals(orderBid5000, savedOrder);
    }

    @Test
    public void shouldGetOrder(){
        given(orderRepository.findById(Mockito.anyLong())).willReturn(Optional.of(orderBid5000));
        Optional<Order> findedOrder = orderService.findById(ID);
        assertTrue(findedOrder.isPresent());
        assertEquals(orderBid5000, findedOrder.get());
    }

    @Test
    public void shouldCancelOrder(){
        doNothing().when(orderRepository).delete(any(Order.class));
        orderService.delete(orderBid5000);
    }

    @Test
    public void shouldGetOrders(){
        List<Order> orders = Arrays.asList(orderBid5000, otherAsk6050, otherAsk6040);
        given(orderRepository.findAll()).willReturn(orders);
        assertEquals(orders.size(), orderService.findAll().size());
    }

    @Test
    public void shouldReturnMatchedSummaryWithOneMatch(){
        given(orderRepository.getOrdersByOrderType(OrderType.ASK)).willReturn(Collections.singletonList(otherAsk6050));
        given(orderRepository.getOrdersByOrderType(OrderType.BID)).willReturn(Collections.singletonList(orderBid5000));
        List<OrderSummary> summary = orderService.matchedSummary();
        assertTrue(!summary.isEmpty());
        assertEquals(1, summary.size());
    }

    @Test
    public void shouldReturnEmptyMatchedSummary(){
        given(orderRepository.getOrdersByOrderType(OrderType.ASK)).willReturn(Collections.singletonList(otherAsk6040));
        given(orderRepository.getOrdersByOrderType(OrderType.BID)).willReturn(Collections.singletonList(orderBid5000));
        assertTrue(orderService.matchedSummary().isEmpty());
    }

    @Test
    public void shouldReturnNotMatchedSummaryWithOneMatch(){
        given(orderRepository.findAll()).willReturn(Collections.singletonList(orderBid5000));
        List<OrderSummary> orderSummaries = orderService.notMatchedSummary();
        assertTrue(!orderSummaries.isEmpty());
        assertEquals(1, orderSummaries.size());
    }

    @Test
    public void shouldReturnEmptyNotMatchedSummary(){
        given(orderRepository.findAll()).willReturn(Arrays.asList(orderBid5000, otherAsk6050));
        List<OrderSummary> orderSummaries = orderService.notMatchedSummary();
        assertTrue(orderSummaries.isEmpty());
    }
}