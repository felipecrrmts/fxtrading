package com.worldfirst.fxtrading.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.worldfirst.fxtrading.assembler.OrderResourceAssembler;
import com.worldfirst.fxtrading.controller.advice.OrderAdvice;
import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderType;
import com.worldfirst.fxtrading.model.builder.OrderBuilder;
import com.worldfirst.fxtrading.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(value = OrderController.class)
public class OrderControllerTests {

	private static String BASE_PATH = "/orders";
	private static Long ID = 123L;

	private MockMvc mockMvc;
	@MockBean private OrderService orderService;
	@MockBean private OrderResourceAssembler orderResourceAssembler;
	@Autowired private ObjectMapper mapper;
	private Order order;

	@Before
	public void setup(){
		mockMvc = MockMvcBuilders.standaloneSetup(new OrderController(orderResourceAssembler,orderService))
				.setControllerAdvice(new OrderAdvice())
				.build();
		setupOrder();
	}

	private void setupOrder(){
		order = new OrderBuilder()
				.withId(ID)
				.withUserId(UUID.randomUUID().toString())
				.withOrderType(OrderType.ASK)
				.withCurrency("GBP/USD")
				.withPrice(new BigDecimal("1.1200"))
				.withAmount(new BigDecimal("5000"))
				.build();
	}

	@Test
	public void getOrderReturnsCorrectResponse() throws Exception {
		given(orderService.findById(ID)).willReturn(Optional.of(order));
		mockMvc.perform(get(BASE_PATH + "/" + ID))
				.andExpect(status().isOk());
	}

	@Test
	public void postReturnsCorrectResponse() throws Exception {

		given(orderService.save(any(Order.class))).willReturn(order);
		mockMvc.perform(
				post(BASE_PATH)
						.content(mapper.writeValueAsBytes(order))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isCreated());
	}

	@Test
	public void postShouldReturn422DueToWrongCurrency() throws Exception {
		Order neworder = new OrderBuilder()
				.withUserId(UUID.randomUUID().toString())
				.withOrderType(OrderType.ASK)
				.withCurrency("GBP/BRL")
				.withPrice(new BigDecimal("1.2100"))
				.withAmount(new BigDecimal("5000"))
				.build();
		mockMvc.perform(
				post(BASE_PATH)
						.content(mapper.writeValueAsBytes(neworder))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void shouldCancelOrder() throws Exception {
		given(orderService.findById(ID)).willReturn(Optional.of(order));
		mockMvc.perform(delete(BASE_PATH + "/" + ID))
				.andExpect(status().isNoContent())
				.andExpect(content().string(""));
	}

	@Test
	public void shouldNotFoundOrderAtCancel() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders.delete("/orders/{id}", 456L))
				.andExpect(status().isNotFound());
	}
	@Test
	public void shouldGetOrderSuccess() throws Exception {
		when(orderService.findById(ID)).thenReturn(Optional.ofNullable(order));
		mockMvc.perform(get("/orders/{id}", 123L))
				.andExpect(status().isOk());

	}
	@Test
	public void getOrderThatDoesNotExistReturnNotFound() throws Exception {
		given(orderService.findById(ID)).willReturn(Optional.empty());
		mockMvc.perform(get(BASE_PATH + "/" + ID))
				.andExpect(status().isNotFound());
	}
//	@Test
//	public void testGetMatchedTrades() throws Exception {
//		Order order1 = new Order("Johnny",OrderType.BID.value(), CurrencyEnum.GBP_USD.value(),"1.1200","3000",new Date(), OrderStatusEnum.PENDING.value());
//		order1.setId(1l);
//		Map<Order,List<Order>> matchedOrdersMap = new HashMap();
//		matchedOrdersMap.put(order1,Arrays.asList(order1));
//		when(orderService.getMatchedOrders()).thenReturn(matchedOrdersMap);
//		mockMvc.perform(get("/orders/matched"))
//				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
//
//	}
//
//	@Test
//	public void testGetUnMatchedTrades() throws Exception {
//		Order order1 = new Order("Johnny",OrderType.BID.value(), CurrencyEnum.GBP_USD.value(),"1.1200","3000",new Date(), OrderStatusEnum.PENDING.value());
//		order1.setId(1l);
//		List<Order> orders = new ArrayList<>();
//		orders.add(order1);
//		when(orderService.getUnMatchedOrders()).thenReturn(orders);
//		mockMvc.perform(get("/orders/unMatched"))
//				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//				.andExpect(jsonPath("$", hasSize(1)));
//	}

}
