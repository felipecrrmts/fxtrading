package com.worldfirst.fxtrading.repository;

import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> getOrdersByOrderType(OrderType orderType);
}
