package com.worldfirst.fxtrading.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.worldfirst.fxtrading.model.validator.DefaultCurrency;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "TRADE_ORDER")
public class Order{

    @Id @GeneratedValue
    private Long id;
    @NotNull
    private String userId;
    @NotNull
    private OrderType orderType;
    @DefaultCurrency
    private String currency;
    @JsonIgnore
    private BigDecimal price;
    @NotNull
    private BigDecimal amount;

    public boolean matchConvertedAmountBidAsk(Order order){
        return !this.orderType.equals(order.getOrderType()) &&
                this.convertAmountByType().equals(order.convertAmountByType());
    }

    public BigDecimal convertAmountByType(){
        if (this.orderType.equals(OrderType.BID))
                return price.setScale(4, BigDecimal.ROUND_HALF_UP)
                        .multiply(amount).setScale(4, BigDecimal.ROUND_HALF_UP);
        return amount.setScale(4, BigDecimal.ROUND_HALF_UP);
    }

}