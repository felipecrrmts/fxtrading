package com.worldfirst.fxtrading.model;

public enum OrderType {
    BID,
    ASK
}
