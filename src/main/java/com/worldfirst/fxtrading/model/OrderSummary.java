package com.worldfirst.fxtrading.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderSummary {

    private Order bidOrder;
    private Order askOrder;
}