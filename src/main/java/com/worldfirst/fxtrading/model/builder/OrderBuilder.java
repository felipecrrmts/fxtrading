package com.worldfirst.fxtrading.model.builder;

import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderType;

import java.math.BigDecimal;

public class OrderBuilder {

    private Order order;

    public OrderBuilder(){
        order = new Order();
    }
    public OrderBuilder withId(Long id){
        this.order.setId(id);
        return this;
    }
    public OrderBuilder withUserId(String userId){
        this.order.setUserId(userId);
        return this;
    }
    public OrderBuilder withOrderType(OrderType orderType){
        this.order.setOrderType(orderType);
        return this;
    }
    public OrderBuilder withCurrency(String currency){
        this.order.setCurrency(currency);
        return this;
    }
    public OrderBuilder withPrice(BigDecimal price){
        this.order.setPrice(price);
        return this;
    }
    public OrderBuilder withAmount(BigDecimal amount){
        this.order.setAmount(amount);
        return this;
    }
    public Order build(){
        return order;
    }
}
