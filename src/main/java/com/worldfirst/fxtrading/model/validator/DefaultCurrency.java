package com.worldfirst.fxtrading.model.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DefaultCurrencyValidator.class)
public @interface DefaultCurrency {

    String message() default "At the moment we are only accepting GBP/USD currency";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String price() default "GBP/USD";
}