package com.worldfirst.fxtrading.model.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class DefaultCurrencyValidator implements ConstraintValidator<DefaultCurrency, String> {

    @Value("${order.currency}")
    private String defaultCurrency;
    @Override
    public void initialize(DefaultCurrency constraintAnnotation) {
        if (this.defaultCurrency == null)
            this.defaultCurrency = constraintAnnotation.price();
    }

    @Override
    public boolean isValid(String currency, ConstraintValidatorContext constraintValidatorContext) {
        return currency != null && currency.equals(defaultCurrency);
    }
}

