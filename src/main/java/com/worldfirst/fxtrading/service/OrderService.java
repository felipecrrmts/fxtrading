package com.worldfirst.fxtrading.service;

import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderSummary;
import com.worldfirst.fxtrading.model.OrderType;
import com.worldfirst.fxtrading.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    @Value("${order.current.price.gbp.usd.value}")
    private String defaultPrice;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public void delete(Order order) {
        orderRepository.delete(order);
    }

    public Order save(Order order) {
        order.setPrice(defaulPriceBigDecimalScaleFour());
        return orderRepository.save(order);
    }

    public List<OrderSummary> matchedSummary() {
        return verifyMatchedSummary(
                orderRepository.getOrdersByOrderType(OrderType.ASK),
                orderRepository.getOrdersByOrderType(OrderType.BID)
        );
    }

    private List<OrderSummary> verifyMatchedSummary(List<Order> askOrders, List<Order> bidOrders) {

        List<OrderSummary> orderSummaries = new ArrayList<>();
        Set<Order> askSet = new LinkedHashSet<>(askOrders);

        bidOrders.forEach(bidOrder -> {
            Optional<Order> optionalAskOrder = askSet.stream()
                    .filter(askOrder -> askOrder.matchConvertedAmountBidAsk(bidOrder))
                    .findFirst();
            optionalAskOrder.ifPresent(askOrder -> {
                orderSummaries.add(OrderSummary.builder()
                        .bidOrder(bidOrder)
                        .askOrder(askOrder)
                        .build());
                askSet.remove(askOrder);
            });
        });

        return orderSummaries;
    }

    private BigDecimal defaulPriceBigDecimalScaleFour(){
        return new BigDecimal(defaultPrice).setScale(4, BigDecimal.ROUND_HALF_UP);
    }

    public List<OrderSummary> notMatchedSummary() {

        return verifyNotMatchedSummary(orderRepository.findAll()).stream()
                .map(order -> OrderSummary.builder().bidOrder(order).build())
                .collect(Collectors.toList());
    }

    private List<Order> verifyNotMatchedSummary(List<Order> orders) {

        Set<Order> orderSet = new LinkedHashSet<>();

        orders.forEach(order -> {
            Optional<Order> optionalOrder = orderSet.stream()
                    .filter(setOrder -> setOrder.matchConvertedAmountBidAsk(order))
                    .findFirst();
            if (optionalOrder.isPresent()) {
                orderSet.remove(optionalOrder.get());
            } else {
                orderSet.add(order);
            }
        });

        return new ArrayList<>(orderSet);
    }

}