package com.worldfirst.fxtrading.controller.advice;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.worldfirst.fxtrading.controller.OrderController;
import com.worldfirst.fxtrading.controller.exception.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

@ControllerAdvice(assignableTypes = OrderController.class)
public class OrderAdvice {

    @Value("${order.not.found.message}")
    private String orderNotFoundMessage;

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<VndErrors> orderNotFoundHandler(OrderNotFoundException e) {
        return error(e, HttpStatus.NOT_FOUND, orderNotFoundMessage + e.getLocalizedMessage());
    }
    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<VndErrors> invalidParamsExceptionHandler(InvalidFormatException e){
        return error(e, HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<VndErrors> invalidParamsExceptionHandler(ConstraintViolationException e){
        return error(e, HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<VndErrors> invalidParamsExceptionHandler(MethodArgumentNotValidException e){
        return error(e, HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<VndErrors> assertionException(final IllegalArgumentException e) {
        return error(e, HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    }

    private ResponseEntity<VndErrors> error(
            final Exception exception, final HttpStatus httpStatus, final String logRef) {
        final String message =
                Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
        return new ResponseEntity<>(new VndErrors(message, logRef), httpStatus);
    }


}