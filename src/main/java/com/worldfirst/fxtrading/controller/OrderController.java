package com.worldfirst.fxtrading.controller;

import com.worldfirst.fxtrading.assembler.OrderResourceAssembler;
import com.worldfirst.fxtrading.controller.exception.OrderNotFoundException;
import com.worldfirst.fxtrading.model.Order;
import com.worldfirst.fxtrading.model.OrderSummary;
import com.worldfirst.fxtrading.service.OrderService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class OrderController {

    private final OrderResourceAssembler orderResourceAssembler;
    private final OrderService orderService;

    public OrderController(OrderResourceAssembler orderResourceAssembler, OrderService orderService) {
        this.orderResourceAssembler = orderResourceAssembler;
        this.orderService = orderService;
    }

    @PostMapping("/orders")
    public ResponseEntity<Resource<Order>> newOrder(@Valid @RequestBody Order newOrder) {
        Order order = orderService.save(newOrder);

        return ResponseEntity
                .created(linkTo(methodOn(OrderController.class).one(order.getId())).toUri())
                .build();
    }

    @GetMapping("/orders/{id}")
    public Resource<Order> one(@PathVariable("id") Long id) {
        return orderResourceAssembler.toResource(
                orderService.findById(id)
                        .orElseThrow(() -> new OrderNotFoundException(id)));
    }

    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resources<Resource<Order>> all() {
        List<Resource<Order>> orders = orderService.findAll().stream()
                .map(orderResourceAssembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(orders,
                linkTo(methodOn(OrderController.class).all()).withSelfRel());
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity cancel(@PathVariable Long id) {
        return orderService
                .findById(id).map(toDeleteOrder -> {
                    orderService.delete(toDeleteOrder);
                    return ResponseEntity.noContent().build();
                })
                .orElseThrow(() -> new OrderNotFoundException(id));
    }

    @GetMapping(value = "/orders-summary/matched")
    public ResponseEntity<List<OrderSummary>> matchedSummary(){

       return new ResponseEntity<>(orderService.matchedSummary(), HttpStatus.OK);
    }

    @GetMapping("/orders-summary/notmatched")
    public ResponseEntity<List<OrderSummary>> notMatchedSummary(){

        return ResponseEntity.ok(orderService.notMatchedSummary());
    }
}