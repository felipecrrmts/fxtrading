package com.worldfirst.fxtrading.controller.exception;

public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(Long id) {
        super(String.valueOf(id));
    }
}
