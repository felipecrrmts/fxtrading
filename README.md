# WorldFirst Java Coding Test - Dummy FX platform

## Tech/Framework used
<b>Scaffolding</b>
- [SPRING INITIALIZR] (https://start.spring.io/)

<b>Built with</b>
- [JDK 8] (http://www.oracle.com/technetwork/java/javase/overview/index.html)
- [Maven] (https://maven.apache.org)
- [Spring Boot 2.1.1] (https://spring.io/projects/spring-boot)
- [Spring Framework 5.0.8] (https://spring.io/projects/spring-framework)
- [Spring Data JPA 2.1.3 ] (https://spring.io/projects/spring-framework)
- [Spring HATEOAS 0.25.0] (https://spring.io/projects/spring-framework)
- [H2 Database Engine] (www.h2database.com)
- [Project Lombok] (https://projectlombok.org)
- [Junit] (https://junit.org/junit4)
- [Mokito] (https://site.mockito.org)
- [Postman] (https://www.getpostman.com)

## Assumptions


- Canceled orders are being deleted.
- BID and ASK are using the same price, because of that order registration doesn't need to receive price
- Summaries are using the rule of no partial matching allowed
- A Postman collection file (`OrderApi.postman_collection.json`) with configured API calls is in the root of the file sent

## Packaging and running

<b>Build</b>
- `build.sh` - issues `mvn clean package` command and produces `fxtrading-0.0.1-SNAPSHOT.jar` in target directory
- `run.sh` - runs build solution jar with command `mvn -U clean compile spring-boot:run`

Runs at:
localhost:8080